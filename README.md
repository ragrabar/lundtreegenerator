# LundTreeGenerator
This is a simple collection of useful functions to make networkx (https://networkx.org) graphs representing
Lund Trees in Python, using NTuples available on the /eos disks from the SM JetPhoton subgroup:

https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SMJetPhotonPhysics, /eos space is `/eos/atlas/atlascerngroupdisk/phys-sm/JetPhoton/`

The code is set up to visualise these graphs nicely (in my opinion), see example below. Red nodes signify primary Lund Plane emissions, and the nodes are numbered in the order of 
Lund declustering, starting from the full jet (0).

![image info](img/ExampleDrawing.png)

To run this code, you need the fastjet python interface, networkx, matplotlib, pydot and some others, but the main function creating the Lund Tree only needs the fastjet python interface (pip3 install fastjet) and networkx (pip3 install networkx).

Run, for example, like this:

`python3 makeLund.py 'path_to_file' -nev 3`

where nev = number of processed events (a single event usually has more than 1 jet).