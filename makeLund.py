import ROOT as root 
import fastjet as fj
import numpy as np 
import networkx as nx
import matplotlib.pyplot as plt
import scipy
import pydot
from networkx.drawing.nx_pydot import graphviz_layout
import argparse

def LundDeclustering(jharder, jsofter):
    # for to PseudoJets, calculate Lund Coordinates.
    # the order and definitions should be the same as in LundNet source code
    lnz = np.float32(np.log(jsofter.pt()/(jharder.pt() + jsofter.pt())))
    delta = jharder.delta_R(jsofter)
    lndelta = np.float32(np.log(delta))
    lnkt = np.float32(np.log(jsofter.pt() * delta))
    lnm = np.float32(0.5 * np.log(abs((jharder + jsofter).m2())))
    psi = np.float32(np.arctan2(jsofter.rap() - jharder.rap(),jsofter.phi() - jharder.phi()))

    return [lnz, lndelta, psi, lnm, lnkt]

def main():
    parser = argparse.ArgumentParser(description='Parser for input file')
    parser.add_argument('ntuplepath', type=str, help='Path to the input file (must be from the JetPhoton SM group eos disk for JSS)')
    parser.add_argument('-nev', '--nevents', type=int, default=2, help='Number of events to process (will take full file if this number exceeds the total number of events)')
    args = parser.parse_args()

    inFileName = args.ntuplepath #"../user.csauer.27842791._000055.ANALYSIS.root"
    inFile = root.TFile.Open(inFileName ,"READ")
    tree = inFile.Get("TrackJSSTrees_JSS")
    totalentries = min(tree.GetEntries(), args.nevents)
    counter = 0
    LundTreeRecoCollection = []
    LundTreeTruthCollection = []
    for entryNum in range(0,totalentries):

        tree.GetEntry(entryNum)
        # number of reconstructed jets
        njet = getattr(tree,     "njet")
        # reco jet variables
        truthidx = getattr(tree, "jet_truthIdx") # index of the matched truth jet
        energies = getattr(tree, "jet_trk0_E")
        pts = getattr(tree,      "jet_trk0_pt")
        etas = getattr(tree,     "jet_trk0_eta")
        phis = getattr(tree,     "jet_trk0_phi")
        # truth jet variables
        tcharge = getattr(tree,  "tjet_constit_q") # will only want charged truth particles
        tenergies = getattr(tree,"tjet_constit_E")
        tpts = getattr(tree,     "tjet_constit_pt")
        tetas = getattr(tree,    "tjet_constit_eta")
        tphis = getattr(tree,    "tjet_constit_phi")

        for i in range(njet): 
            # fastjet
            LundTreeRecoCollection.append(networkx_from_constits(energies[i], pts[i], etas[i],phis[i], \
                                          charges = None, \
                                          follow_primary = True))
            itruth = truthidx[i]
            LundTreeTruthCollection.append(networkx_from_constits(tenergies[itruth], tpts[itruth], tetas[itruth], tphis[itruth], \
                                           charges = tcharge[itruth], \
                                           follow_primary = True))
#    for LundTree in LundTreeRecoCollection:
#        plt.clf()
#        counter += 1
#        print(LundTree.nodes(data="follow_primary"))
#        color_map = ['red' if node[1] == "primary" else 'blue' for node in LundTree.nodes(data="follow_primary")] 
#        pos = graphviz_layout(LundTree, prog="dot")
#        nx.draw(LundTree, pos, with_labels=True, node_color=color_map)
#        plt.savefig('Lund' + str(counter) + '.pdf')
#    counter = 0
#    for LundTree in LundTreeTruthCollection:
#        plt.clf()
#        counter += 1
#        color_map = ['red' if node[1] == "primary" else 'blue' for node in LundTree.nodes(data="follow_primary")] 
#        pos = graphviz_layout(LundTree, prog="dot")
#        nx.draw(LundTree, pos, with_labels=True, node_color=color_map)
#        plt.savefig('Lund' + str(counter) + 'Truth.pdf')
    for i in range(len(LundTreeTruthCollection)):
        plt.clf()
        LundTruthTree = LundTreeTruthCollection[i]
        color_truth_map = ['red' if node[1] == "primary" else 'white' for node in LundTruthTree.nodes(data="follow_primary")] 
        postruth = graphviz_layout(LundTruthTree, prog="dot")

        LundRecoTree = LundTreeRecoCollection[i]
        color_reco_map = ['red' if node[1] == "primary" else 'white' for node in LundRecoTree.nodes(data="follow_primary")] 
        posreco = graphviz_layout(LundRecoTree, prog="dot")

        plt.subplot(121)
        plt.title("Truth Lund Tree")
        nx.draw(LundTruthTree, postruth, with_labels=True, node_color=color_truth_map)

        plt.subplot(122)
        plt.title("Reco Lund Tree")
        nx.draw(LundRecoTree, posreco, with_labels=True, node_color=color_reco_map)
        plt.savefig("RecoTruthLund" + str(i) + ".pdf")
    print(counter)

def networkx_from_constits(Es, pts, etas, phis, charges = None, follow_primary = False):
    jet_def = fj.JetDefinition(fj.cambridge_algorithm, 1000.0)

    if charges is not None:
        filter_array = lambda array_to_filter, charge_array: [x for x, c in zip(array_to_filter, charge_array) if c != 0]
        Es =   filter_array(Es, charges)
        pts =  filter_array(pts, charges)
        etas = filter_array(etas, charges)
        phis = filter_array(phis, charges)

    input_pjs = [fj.PseudoJet(pts[i]*np.cos(phis[i]), pts[i]*np.sin(phis[i]), pts[i]*np.sinh(etas[i]), Es[i]) \
                 for i in range(len(Es))]
    # 22.08.2023: attempt at looking directly at the cs.jets() list
    # and creating a Lund Tree from it
    cs = fj.ClusterSequence(input_pjs, jet_def)
    jetlist = cs.jets()[len(input_pjs):] # the PseudoJets cut out here are just input_pjs
    jetlist = jetlist[::-1] # now the first element = full pseudojet
    if len(jetlist) == 0:
        return nx.Graph()
    if follow_primary:
        hardguys = [jetlist[0]]

    assert len(jetlist) == len(input_pjs) - 1 # if cs.jets() works correctly, must be the case

    LundTree = nx.Graph()
    
    for i_j in range(len(jetlist)):
        # check if the jet is a result of a recombination
        # the first n entries in jetlist are just input_pjs
        # so they should have been cut out by now anyway

        current_jet = jetlist[i_j]
        child = fj.PseudoJet()
        parharder = fj.PseudoJet()
        parsofter = fj.PseudoJet()
        if not current_jet.has_parents(parharder,parsofter): continue
        
        if parsofter.pt() > parharder.pt(): parharder, parsofter = parsofter, parharder

        LundCoord = LundDeclustering(parharder,parsofter)
        LundTree.add_node(i_j, coordinates = LundCoord[:2], features = LundCoord)

        if follow_primary:
            hardchild = fj.PseudoJet()
            if (not parharder.has_child(hardchild)) or (hardchild in hardguys):
                hardguys.append(parharder)
                LundTree.nodes[i_j]["follow_primary"] = "primary"
            else:
                LundTree.nodes[i_j]["follow_primary"] = "secondary"

        if current_jet.has_child(child):
            LundTree.add_edge(jetlist.index(child), i_j)

    return LundTree

if __name__ == '__main__':
    root.gROOT.SetBatch()
    main()